//
//  CreateTeamPage.swift
//  Squadix
//
//  Created by Illia Romanenko on 2.04.21.
//  Copyright © 2021 Illia Romanenko. All rights reserved.
//

import UIKit

class CreateTeamPage: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Создание команды"
    }

}
